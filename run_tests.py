import subprocess
import sys

def run_tests():
    try:
        # Run the test_calculator.py file
        result = subprocess.run(['python', '-m', 'unittest', 'test_calculator.py'], check=True)
    except subprocess.CalledProcessError:
        sys.exit(1)

if __name__ == '__main__':
    run_tests()
