import unittest
from calculator import Calculator

class TestCalculator(unittest.TestCase):
    def setUp(self):
        self.calculator = Calculator()

    def test_add(self):
        result = self.calculator.add(3, 5)
        self.assertEqual(result, 8)
        print(f"Test add passed. Result: {result}")

    def test_subtract(self):
        result = self.calculator.subtract(8, 4)
        self.assertEqual(result, 4)
        print(f"Test subtract passed. Result: {result}")

    def test_multiply(self):
        result = self.calculator.multiply(2, 6)
        self.assertEqual(result, 12)
        print(f"Test multiply passed. Result: {result}")

    def test_divide(self):
        result = self.calculator.divide(10, 2)
        self.assertEqual(result, 5)
        print(f"Test divide passed. Result: {result}")

    def test_divide_by_zero(self):
        with self.assertRaises(ValueError):
            self.calculator.divide(5, 0)
        print("Test divide_by_zero passed. Exception raised as expected.")

if __name__ == '__main__':
    unittest.main()
